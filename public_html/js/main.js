new Vue({
    el: '#app',
    data:{
        title: "Hello",
        testDom: "Это с дома",
        styleCSS: ''
    },
    methods:{
        changeText(){
            this.title='Броооо'
        },
        testMethods(){
            this.testDom = 'Домашняя работа'
        }
    }
    
});

new Vue({
el: '#app_2',
data:{
    value: 1,
    //doubleValue: 1
},
methods:{
    increment(val){
        this.value = val
        //this.doubleValue = val * 2
        if (val == 25){
            alert ("Число 25!")
        }
    }
},
computed: {
    doubleValue(){
        return this.value * 2
    }
}
});

new Vue({
    el:'#app_3',
        data:{
        message: 'Hello!!!!!!!!!!!!!!!!!!!',
        show: true,
        cars:[
            {model: "BMW", speed:250},
            {model: "Audi", speed:200},
            {model: "Mersedes", speed:180},
            {model: "Reno", speed:160}
        ]
    },
    computed: {
        showMess (){
            return this.message.toUpperCase()
        }
    },
    filters:{
        lowercase(value){
            return value.toLowerCase()
        }
    }

});

//глобальный фильтр
Vue.filter('capitalize', function(param){
    if(!param) return ''
    param = param.toString()
    return param.replace(/\b\w/g, function(l){return l.toUpperCase()})
});

new Vue({
    el: '#app_4',
    data:{
        inputValue: 'Primer'
    }

});

new Vue({
    el:'#app-3',
    data:{
        name:'Vue!!!',
        a:10,
        b:5,
        forIf: true,
        forSplit:'Меня зовут Саша'

    },
    methods:{
      summa: function(){
        return (this.a + this.b*2)/77
    } 
    } 

});

new Vue({
    el:'#app-4',
    data:{
       url: 'http://google.com'
    }
});

new Vue({
    el:'#app-5',
    data:{
        link:'<a href="http://google.com"> Google </a>'

    }
});

new Vue({
    el:'#app-6',
    data:{
        counter: 0
    },
    methods:{
        clic: function(){
            this.counter++    
        },
        onHover: function(event){
            event.target.style.color = 'red'
        }
    }
});

new Vue({
    el:'#app-7',
    data:{
       counter: 0,
       title: 'Счетчик'
    },
    methods:{
        go: function(a, event){
            this.counter += a;
            this.title = 'Счетчик увеличен на ' + a;   

            //blue
            if(a === 5){
                event.target.style.color = 'blue'
            }
            //red
            else if(a === 10){
                event.target.style.color = 'red'
            }
        }


    }
});

new Vue({
    el:'#app-11',
    data:{
        inputValue: 'Заполните поле'
    },
    methods:{

    }
});

new Vue({
    el:'#app-12',
    data:{
        isActive: false,
        color: 'pink'
    },
    methods:{
        
    },
    computed:{
        getCssClasses: function(){
            return{
                       'red' : this.isActive,
                       'green' : !this.isActive     
            }
        }
    }
});

new Vue({
    el:'#app-13',
    data:{
        color: 'blue',
        height: 100
    },
    methods:{
        
    },
    computed:{
        circleClasses: function(){
            return{
                'background' : this.color,
                'height' : this.height + 'px'
            }
        }

    }
});

new Vue({
    el:'#app-14',
    data:{
        isVisible: true
    },
    methods:{
        
    },
    computed:{

    }
});

new Vue({
    el:'#app-16',
    data:{
        people: [
            {name: 'Max', age: 19},
            {name: 'Vlad', age: 20},
            {name: 'Elena', age: 17}
        ],
        rabotnik:{
            name:'Max',
            age: 30,
            job: 'frontend'
        }
    },
    methods:{
        
    },
    computed:{

    }
});

new Vue({
    el:'#app-17',
    data:{
        counter: 0,
        condition: ''
    },
    methods:{
        add:function(){
            this.counter++
            this.condition = (this.counter > 3) ? 'Больше 3' : 'Меньше 3'
        },
        sub:function(){
            this.counter--
            this.condition = (this.counter > 3) ? 'Больше 3' : 'Меньше 3'
        }
        
    },
    computed:{

    }
});